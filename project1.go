package main

import (
        "github.com/kraman/go-firmata"
        "time"
        "fmt"
        "regexp"
        "os"
        "os/signal"
        "syscall"
        "strings"
)

func main() {

    var led uint8 = 13
    var morseCodeLookup = map[string]string{
        "a":  ".-",
        "b":  "-...",
        "c":  "-.-.",
        "d":  "-..",
        "e":  ".",
        "f":  "..-.",
        "g":  "--.",
        "h":  "....",
        "i":  "..",
        "j":  ".---",
        "k":  "-.-",
        "l":  ".-..",
        "m":  "--",
        "n":  "-.",
        "o":  "---",
        "p":  ".--.",
        "q":  "--.-",
        "r":  ".-.",
        "s":  "...",
        "t":  "-",
        "u":  "..-",
        "v":  "...-",
        "w":  ".--",
        "x":  "-..-",
        "y":  "-.--",
        "z":  "--..",
        "0":  "-----",
        "1":  ".----",
        "2":  "..---",
        "3":  "...--",
        "4":  "....-",
        "5":  ".....",
        "6":  "-....",
        "7":  "--...",
        "8":  "---..",
        "9":  "----.",
    }
    // determine if alphanumeric 
    var isStringAlphabetic = regexp.MustCompile(`^[a-zA-Z0-9]*$`).MatchString
    arduino, err := firmata.NewClient("/dev/cu.usbmodem1101", 57600)
    if err != nil {
        panic(err)
    }
    dotDuration := time.Millisecond * 200
    sigs := make(chan os.Signal, 1)
    for {
        signal.Notify(sigs, syscall.SIGINT, syscall.SIGTERM, syscall.SIGTSTP)
        go func() {
            sig := <-sigs
            fmt.Println()
            fmt.Println(sig)
            fmt.Println("Closing Program ... GoodBye")
            arduino.Close()
            os.Exit(3)
        }()
        fmt.Println("Welcome to the GoLang Morse Code LED System")
        fmt.Println("Please enter a sting of good inputs or enter the sentinal !")
        fmt.Print("Enter string: ")
        var input string
        fmt.Scanln(&input)
        if isStringAlphabetic(input) {
            input = strings.ToLower(input)
            arduino.SetPinMode(led, firmata.Output)
            length := len(input)
            for x := 0; x < length; x++ {
                if (string(input[x]) == " ") {
                    arduino.Delay(dotDuration * 7) // Space Between Words
                } else {
                var morseCode = morseCodeLookup[string(input[x])]
                lengthCode := len(morseCode)
                for y := 0; y < lengthCode; y++ {
                    var character = string(morseCode[y])
                    arduino.DigitalWrite(led, true)
                    if (character == ".") {
                        arduino.Delay(dotDuration)
                    } else {
                        arduino.Delay(dotDuration * 3)
                    }
                        arduino.DigitalWrite(led, false)
                    }
                    arduino.Delay(dotDuration * 3) // Space Between Letters
                }
                led = led - 1
                if (led < 11) {
                    led = 13
                }
            }
        } 
        if (input == "!") {
            fmt.Println("Closing Program ... GoodBye")
            arduino.Close()
            os.Exit(3)
        }
    }
}