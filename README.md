# EN.605.715.81.FA21.project1

This Project keyboard string inputs and translates into morse code and transmits it to an arudino uno connected to a bread board and flash an one of three leds per word. This program makes use of go firmata

Based on the article https://en.wikipedia.org/wiki/Morse_code#:~:text=Morse%20code%20is%20a%20method,dashes%2C%20or%20dits%20and%20dahs.&text=Each%20dit%20or%20dah%20within,equal%20to%20the%20dit%20duration.

We have a dot duration, a dash duration, a gap between words, and a gap between letters.
For intensive purposes the dot duration has been defined as 200 * miliseconds, a dash is 3x a dot duration, a letter gap is 3x a dot duration, and a space is 7x dot duration.

To run this program you must run

go get github.com/kraman/go-firmata

update the pathing to the arudino should be line 57 
firmata.NewClient("/dev/cu.usbmodem1101", 57600)

go run project1.go

the program can be exited with ctrl + c or ctrl + z or !